# Auto-Hide Completed Levels

Automatically enables the "hide completed" option for Steam Workshop levels in Train Valley 2.

[![latest version](https://img.shields.io/gitlab/v/tag/isekai%252Ftrain-valley-2-modding/AutoHideCompletedLevels?label=Latest&sort=semver&style=for-the-badge)](https://gitlab.com/isekai/train-valley-2-modding/AutoHideCompletedLevels/-/releases)
[![pipeline status](https://img.shields.io/gitlab/pipeline/isekai/train-valley-2-modding/AutoHideCompletedLevels/main?style=for-the-badge)](https://gitlab.com/isekai/train-valley-2-modding/AutoHideCompletedLevels/-/pipelines/main/latest)
[![lines of code](https://img.shields.io/tokei/lines/gitlab/isekai%252Ftrain-valley-2-modding/AutoHideCompletedLevels?style=for-the-badge)](https://gitlab.com/isekai/train-valley-2-modding/AutoHideCompletedLevels)

## Installation instructions

1. Download and install Train Valley 2 from Steam
2. Download and install the latest stable x64 release of [BepInEx 5](https://docs.bepinex.dev/articles/user_guide/installation/index.html)
   into your Train Valley 2 installation directory
3. Download the latest release
   of [AutoHideCompletedLevels](https://gitlab.com/isekai/train-valley-2-modding/AutoHideCompletedLevels/-/releases)
4. Extract the contents of the archive into your Train Valley 2 installation directory
5. (Optional) Download and install the latest release of [Configuration Manager](https://github.com/BepInEx/BepInEx.ConfigurationManager)
   into your Train Valley 2 installation directory
   - Access settings with F1; allows you to disable the mod without uninstalling it entirely

## Release notes

Release notes can be found in [CHANGELOG.md](./CHANGELOG.md).

## Build instructions

1. Download and install Train Valley 2 from Steam
2. Download and install the latest stable x64 release of [BepInEx 5](https://docs.bepinex.dev/articles/user_guide/installation/index.html)
   into your Train Valley 2 installation directory
3. Download and install the [.NET 5.0 SDK](https://dotnet.microsoft.com/download)
   - For Arch Linux, the [`dotnet-sdk-bin` AUR package](https://aur.archlinux.org/packages/dotnet-sdk-bin/) can be used
4. Set the `TRAIN_VALLEY_2_HOME` environment variable to your Train Valley 2 installation directory
   - Should contain the main game executable inside
      - Windows: `TrainValley2.exe`
      - macOS: `TrainValley2.app`
      - Linux: `TrainValley2.x86_64`
   - For Linux, add the following `export` line to `~/.profile` (creating the file if it doesn't exist):
      ```sh
      export TRAIN_VALLEY_2_HOME="<<path-to-game-installation-directory>>"
      ```
5. Open the solution in your IDE of choice, or restart it if it was already running so the new environment variable is picked up
   - Note that in the case of JetBrains Rider, `File -> Invalidate Caches... -> Just restart` is not sufficient
   - On Linux, you may need to logout and re-login to your desktop session
6. Build the solution using your IDE

### Build tasks

#### Pre-build

- Copies the necessary assemblies from the game directory

#### Post-build

- Copies the compiled `.dll` to the game directory, reading for testing

## Developing

1. Setup your environment as per the build instructions above
2. Download and install the latest release of [ScriptEngine](https://github.com/BepInEx/BepInEx.Debug) into your Train Valley 2 installation
   directory
3. Run the following (from the root of the repository) to enable git hooks:
   - macOS / Linux:
      ```sh
      chmod u+x scripts/setup-git-hooks.sh
      ./scripts/setup-git-hooks.sh
      ```
   - Windows (future updates will not auto-apply; run again to apply them manually):
      ```powershell
      ./scripts/setup-git-hooks.ps1
      ```

### Git hooks

#### Pre-commit

- Aggregate individual release notes files (under `doc/release-notes`) into a combined file at `CHANGELOG.md`
