# Release Notes

## 1.0.0

- ➕ Initial release version

## 0.1.0

- ➕ Initial pre-release version
