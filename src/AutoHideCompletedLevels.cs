﻿using System.Diagnostics.CodeAnalysis;
using BepInEx;
using BepInEx.Configuration;
using BepInEx.Logging;
using HarmonyLib;
using Menu.Workshop;
using UnityEngine.UI;

namespace Dev.Isekai.TrainValley2.AutoHideCompletedLevels
{
    [BepInPlugin(PluginInfo.PLUGIN_GUID, PluginInfo.PLUGIN_NAME, PluginInfo.PLUGIN_VERSION)]
    [BepInProcess(EXE_WINDOWS)]
    [BepInProcess(EXE_MACOS)]
    [BepInProcess(EXE_LINUX)]
    public class AutoHideCompletedLevels : BaseUnityPlugin
    {
        private const string EXE_WINDOWS = "TrainValley2.exe";
        private const string EXE_MACOS = "TrainValley2.app";
        private const string EXE_LINUX = "TrainValley2.x86_64";

        [SuppressMessage("ReSharper", "InconsistentNaming", Justification = "Shadows the non-static base field")]
        private new static ManualLogSource? Logger;

        [SuppressMessage("ReSharper", "InconsistentNaming", Justification = "Shadows the non-static base field")]
        private new static Configuration? Config;

        private static Harmony? _hooks;

        private void Awake()
        {
            Logger = base.Logger;
            Config = new(base.Config);
            _hooks = Harmony.CreateAndPatchAll(typeof(Hooks));

            Logger.LogInfo("Finished loading.");
        }

        private void OnDestroy()
        {
            _hooks?.UnpatchSelf();
        }

        private class Configuration
        {
            private readonly ConfigEntry<bool> _enabled;

            internal bool Enabled => _enabled.Value;

            internal Configuration(ConfigFile config)
            {
                _enabled = config.Bind("General", "Enabled", true, "Is the mod enabled?");
            }
        }

        [SuppressMessage("ReSharper", "InconsistentNaming",
            Justification = "Harmony uses magic parameter names for hooking into the game's functions"
        )]
        private static class Hooks
        {
            [HarmonyPatch(typeof(WorkshopMenu), "Awake")]
            [HarmonyPostfix]
            private static void OnMainMenuStart(ref Toggle ____hidePlayed)
            {
                if (Config is { Enabled: true }) {
                    ____hidePlayed.isOn = true;
                }
            }
        }
    }
}
